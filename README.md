**IMPORTANT NOTE: This code has been merged with renpy [8.1.1 and above](https://github.com/renpy/renpy/commit/2de0ec9bee08bea5c9a0955917cc7a119e3ee820).**

# RenPy Thai Font Adjuster

Thai Font Adjuster for [Ren'Py](https://github.com/renpy/renpy)

This code is heavily inspired from [Unity3D.ThaiFontAdjuster](https://github.com/SaladLab/Unity3D.ThaiFontAdjuster)

Works with renpy-7.4.10-sdk

![features](docs/features.png)

## How to use

1. past `renpythaic90.py` in `%your renpy sdk%/renpy/text/`

1. edit `%your renpy sdk%/renpy/text/text.py`

1. past text `import renpy.text.renpythaic90 as renpythaic90` near line 34.

1. near line 246 ~ 256th you will see `if self.ignore:return [ ]`, past text `s = renpythaic90.thai_adjuster(s)` below it. 
like this
```
    # From here down is the public glyph API.

    def glyphs(self, s, layout):
        """
        Return the list of glyphs corresponding to unicode string s.
        """
        if self.ignore:
            return [ ]

        s = renpythaic90.thai_adjuster(s) 
        # ^^^^^ past above here
```

1. done. enjoin your game.

## Limitation

#### Only can handle special fonts.
- Font which meets ~~[C90 encoding for Thai]()~~. (original link is dead and got hijack; so search it yourself) (Have extended glyphs from U+F700 to U+F71A providing various position of Thai characters.)
- [NECTEC National Fonts](http://www.nectec.or.th/pub/review-software/font/national-fonts.html) (Garuda, Loma, Kinnari, Norasi)

#### Adjusting position is not same as rendered one with GPOS and GSUB support.
