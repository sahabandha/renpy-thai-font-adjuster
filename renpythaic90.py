# MIT License

# Copyright (c) 2021 SahabandhSthabara, Saamkhaih Kyakya

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# http://www.bakoma-tex.com/doc/fonts/enc/c90/c90.pdf
# ========== EXTENDED CHARACTER TABLE ==========
# F700:     uni0E10.descless    (base.descless)
# F701~04:  uni0E34~37.left     (upper.left)
# F705~09:  uni0E48~4C.lowleft  (top.lowleft)
# F70A~0E:  uni0E48~4C.low      (top.low)
# F70F:     uni0E0D.descless    (base.descless)
# F710~12:  uni0E31,4D,47.left  (upper.left)
# F713~17:  uni0E48~4C.left     (top.left)
# F718~1A:  uni0E38~3A.low      (lower.low)
# ==============================================

def isBase(c):
    return (u'\u0E01' <= c <= u'\u0E30') or c == u"\u0E30" or c == u"\u0E40" or c == u"\u0E41"

def isBaseAsc(c):
    return c == u'\u0E1B' or c == u'\u0E1D' or c == u'\u0E1F' or c == u'\u0E2C'

def isBaseDesc(c):
    return c == u'\u0E0E' or c == u'\u0E0F'

def isTop(c):
    # Tone Mark, THANTHAKHAT
    if u"\u0E48" <= c <= u"\u0E4C":
        return True

def isLower(c):
    #SARA U, SARA UU, PHINTHU
    return c >= u"\u0E38" and c <= u"\u0E3A"

def isUpper(c):
    return c == u'\u0E31' or c == u'\u0E34' or c == u'\u0E35' or c == u'\u0E36' or c == u'\u0E37' or c == u'\u0E47' or c == u'\u0E4D'

def thai_adjuster(s):

    ss = u""

    # [sara am] -> [nikhahit] [sara aa]
    s = s.replace(u"\u0E33", u"\u0E4D\u0E32")
    s = s.replace(u"\u0E48\u0E4D", u"\u0E4D\u0E48")
    s = s.replace(u"\u0E49\u0E4D", u"\u0E4D\u0E49")
    s = s.replace(u"\u0E4A\u0E4D", u"\u0E4D\u0E4A")
    s = s.replace(u"\u0E4B\u0E4D", u"\u0E4D\u0E4B")
    s = s.replace(u"\u0E4C\u0E4D", u"\u0E4D\u0E4C")

    length = len(s)
    for z in range(length):
        c = s[z]

        #  [base] ~ [top]
        if isTop(c) and z > 0:
                # [base]             [top] -> [base]             [top.low]
                # [base]     [lower] [top] -> [base]     [lower] [top.low]
                # [base.asc]         [top] -> [base.asc]         [top.lowleft]
                # [base.asc] [lower] [top] -> [base.asc] [lower] [top.lowleft]
            b = s[z - 1];
            if isLower(b) and z > 0:
                b = s[z -2]
            if isBase(b):
                Nikhahit = (z < length - 1 and (s[z + 1] == u'\u0E33' or s[z + 1] == u'\u0E4D'))
                if isBaseAsc(b):
                    if Nikhahit:
                        # [base.asc] [nikhahit] [top] -> [base.asc] [nikhahit] [top.left]
                        choices = {
                            u'\u0E48': u'\uF713',
                            u'\u0E49': u'\uF714',
                            u'\u0E4A': u'\uF715',
                            u'\u0E4B': u'\uF716',
                            u'\u0E4C': u'\uF717'
                            }
                        c = choices.get(c, 'error')
                    else:
                        choices = {
                            u'\u0E48': u'\uF705',
                            u'\u0E49': u'\uF706',
                            u'\u0E4A': u'\uF707',
                            u'\u0E4B': u'\uF708',
                            u'\u0E4C': u'\uF709'
                            }
                        c = choices.get(c, 'error')
                else:
                    if Nikhahit == False:
                        choices = {
                            u'\u0E48': u'\uF70A',
                            u'\u0E49': u'\uF70B',
                            u'\u0E4A': u'\uF70C',
                            u'\u0E4B': u'\uF70D',
                            u'\u0E4C': u'\uF70E'
                            }
                        c = choices.get(c, 'error')
            # [base.asc] [upper] [top] -> [base.asc] [upper] [top.left]
            if (z > 1 and isUpper(s[z -1]) and isBaseAsc(s[z - 2])):
                choices = {
                    u'\u0E48': u'\uF713', 
                    u'\u0E49': u'\uF714', 
                    u'\u0E4A': u'\uF715',
                    u'\u0E4B': u'\uF716',
                    u'\u0E4C': u'\uF717'
                    }
                c = choices.get(c, 'error')
        # [base.asc] [upper] -> [base.asc] [upper-left]
        elif (isUpper(c)and z > 0 and isBaseAsc(s[z -1])):
            choices = {
                u'\u0E31': u'\uF710',
                u'\u0E34': u'\uF701',
                u'\u0E35': u'\uF702',
                u'\u0E36': u'\uF703',
                u'\u0E37': u'\uF704',
                u'\u0E4D': u'\uF711',
                u'\u0E47': u'\uF712'
                }
            c = choices.get(c, 'error')
        elif (isLower(c) and z > 0 and isBaseDesc(s[z -1])):
            choices = {
                u'\u0E38': u'\uF718', 
                u'\u0E39': u'\uF719', 
                u'\u0E3A': u'\uF71A'
                }
            c = choices.get(c, 'error')
        elif (c == u'\u0E0D' and z < length -1 and isLower(s[z + 1])):
            c = u'\uF70F'
        elif (c == u'\u0E10' and z < length -1 and isLower(s[z + 1])):
            c = u'\uF700'
        else:
            c = s[z]

        ss = ss + c

    return ss
